package com.todo.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TodoTaskPortTest {

  private TodoTaskPort todoService;
  private TodoListPort listService;
  private TodoRepository repository;

  @BeforeEach
  void setUp() {
    this.repository = new StubTodoRepository();
    this.todoService = TodoPortFactory.createTodoTaskService(this.repository);
    this.listService = TodoPortFactory.createTodoListService(this.repository);
  }

  @Test
  void shouldAddTodoTaskToList() {
    var description = "Task_description";

    UUID listId = this.createNewTodoList();
    this.todoService.addTodoTaskToList(listId, description);

    List<TodoTask> taskList = this.repository.read().get(0).getTodoTasks();
    assertEquals(1, taskList.size(), "Todo list should have one task");
    TodoTask task = taskList.get(0);
    assertNotNull(task.getTaskId(), "Task should have identifier");
    assertEquals(description, task.getDescription(), "Task should have given description");
    assertFalse(task.getCompleted(), "Task should be created completed set to false");
  }

  @Test
  void shouldMarkTaskAsCompleted() {

    UUID listId = this.createNewTodoList();
    UUID taskId = this.todoService.addTodoTaskToList(listId, "description");

    this.todoService.markTaskCompleted(listId, taskId);

    TodoList todoList = this.repository.read(listId).get();
    assertTrue(todoList.getTodoTasks().get(0).getCompleted(), "Task should be marked as done");
  }

  private UUID createNewTodoList() {
    return this.listService.createTodoList();
  }
}
