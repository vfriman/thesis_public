package com.todo.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class TodoListPortTest {

  private TodoListPort todoService;
  private TodoRepository repository;

  @BeforeEach
  void setUp() {
    this.repository = new StubTodoRepository();
    this.todoService = TodoPortFactory.createTodoListService(this.repository);
  }

  @Test
  void shouldCreateNewTodoList() {

    UUID createdListId = this.todoService.createTodoList();

    assertNotNull(createdListId, "Service returned null identifier for list");
    assertEquals(1, this.repository.read().size(), "Repository should have one todo list");
  }

  @Test
  void shouldReadTodoLists() {

    assertTrue(this.todoService.readTodoLists().isEmpty(), "Service should return empty list");
    this.todoService.createTodoList();
    this.todoService.createTodoList();
    assertEquals(2, this.todoService.readTodoLists().size(), "Service should return newly create list");
  }

  @Test
  void shouldFetchSingleTodoList() {

    this.todoService.createTodoList();
    UUID listId = this.todoService.createTodoList();

    TodoList todoList = this.todoService.fetchTodoList(listId)
      .orElseGet(() -> {
        throw new AssertionError("Expected to found todo list with id");
      });

    assertEquals(listId, todoList.getListId(), "List id should match");
  }

}
