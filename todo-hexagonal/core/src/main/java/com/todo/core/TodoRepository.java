package com.todo.core;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 *  Repository port for technology specific
 *  implementation.
 *
 */
public interface TodoRepository {

  /**
   * Saves todo list
   *
   * @param todoList
   */
  void save(final TodoList todoList);

  /**
   * Returns all the lists. Returns empty list if none is found.
   *
   * @return
   */
  List<TodoList> read();

  /**
   * Returns todo list with given id.
   *
   * @param listId
   * @return
   */
  Optional<TodoList> read(final UUID listId);
}
