package com.todo.core;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

class TodoService implements TodoTaskPort, TodoListPort {

  private final TodoRepository repository;

  TodoService(final TodoRepository repository) {
    this.repository = repository;
  }

  @Override
  public UUID createTodoList() {
    TodoList todoList = TodoList.builder().listId(UUID.randomUUID()).todoTasks(Collections.emptyList()).build();
    this.repository.save(todoList);

    return todoList.getListId();
  }

  @Override
  public List<TodoList> readTodoLists() {
    return this.repository.read();
  }

  @Override
  public Optional<TodoList> fetchTodoList(final UUID todoListiId) {
    return this.repository.read(todoListiId);
  }

  @Override
  public UUID addTodoTaskToList(final UUID todolistId, final String taskDescription) {
    TodoList todoList = this.repository.read(todolistId).get();

    final TodoTask todoTask = TodoTask.builder().taskId(UUID.randomUUID())
      .listId(todolistId).completed(FALSE).description(taskDescription).build();

    List<TodoTask> updatedTaskList = new ArrayList<>(todoList.getTodoTasks());
    updatedTaskList.add(todoTask);

    this.repository.save(TodoList.builder().listId(todoList.getListId()).todoTasks(updatedTaskList).build());

    return todoTask.getTaskId();
  }

  @Override
  public void markTaskCompleted(final UUID todolistId, final UUID taskid) {
    Optional<TodoList> todoListOptional = this.repository.read(todolistId);
    if (todoListOptional.isPresent()) {
      TodoList todoList = todoListOptional.get();
      Optional<TodoTask> possibleTodoTask = todoList.getTodoTasks().stream()
        .filter(task -> Objects.equals(task.getTaskId(), taskid))
        .findFirst();
      if (possibleTodoTask.isPresent()) {
        List<TodoTask> updatedTaskList = todoList.getTodoTasks().stream()
          .filter(task -> !Objects.equals(task.getTaskId(), taskid))
          .collect(Collectors.toList());
        updatedTaskList.add(TodoTask.builder().taskId(taskid).listId(todolistId).completed(TRUE)
          .description(possibleTodoTask.get().getDescription()).build());
        this.repository.save(TodoList.builder().listId(todoList.getListId()).todoTasks(updatedTaskList).build());
      }
    }

  }
}
