package com.todo.core;

import java.util.Objects;

/**
 * A factory class which knows how to assemble TodoServices
 */
public class TodoPortFactory {

  public static TodoTaskPort createTodoTaskService(final TodoRepository todoRepository) {
    Objects.requireNonNull(todoRepository, "Repository implementation cannot be null");

    return new TodoService(todoRepository);
  }

  public static TodoListPort createTodoListService(final TodoRepository todoRepository) {
    Objects.requireNonNull(todoRepository, "Repository implementation cannot be null");

    return new TodoService(todoRepository);
  }
}
