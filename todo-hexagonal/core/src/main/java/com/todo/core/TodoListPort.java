package com.todo.core;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TodoListPort {

  /**
   * Creates new Todo list
   *
   * @return Returns UUID of created todo list
   */
  UUID createTodoList();

  /**
   * Returns all todo lists
   *
   * @return
   */
  List<TodoList> readTodoLists();

  /**
   * Returns todo list with given id.
   *
   * @param todoListiId
   * @return
   */
  Optional<TodoList> fetchTodoList(final UUID todoListiId);
}
