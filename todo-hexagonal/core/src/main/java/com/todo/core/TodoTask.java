package com.todo.core;

import java.util.Objects;
import java.util.UUID;

/**
 * Data transfer object which represents a task in todo list
 */
public final class TodoTask {
  private final UUID taskId;
  private final UUID listId;
  private final Boolean completed;
  private final String description;

  private TodoTask(final UUID taskId, final UUID listId, final Boolean completed, final String description) {
    this.taskId = taskId;
    this.listId = listId;
    this.completed = completed;
    this.description = description;
  }

  /**
   * Returns task's identifier
   *
   * @return
   */
  public UUID getTaskId() {
    return taskId;
  }

  /**
   * Returns identifier of the list which this task belongs to.
   *
   * @return
   */
  public UUID getListId() {
    return listId;
  }

  /**
   * Returns if task is marked as completed.
   *
   * @return
   */
  public Boolean getCompleted() {
    return completed;
  }

  /**
   * Returns task's description.
   *
   * @return
   */
  public String getDescription() {
    return description;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final TodoTask todoTask = (TodoTask) o;
    return taskId.equals(todoTask.taskId) &&
      listId.equals(todoTask.listId) &&
      completed.equals(todoTask.completed) &&
      description.equals(todoTask.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(taskId, listId, completed, description);
  }

  @Override
  public String toString() {
    return "TodoTask{" +
      "taskId=" + taskId +
      ", listId=" + listId +
      ", completed=" + completed +
      ", description='" + description + '\'' +
      '}';
  }

  public static TodoTaskBuilder builder() {
    return new TodoTaskBuilder();
  }

  public static TodoTaskBuilder builder(final TodoTask orginal) {
    return new TodoTaskBuilder(orginal);
  }

  public static class TodoTaskBuilder {
    private UUID taskId;
    private UUID listId;
    private Boolean completed;
    private String description;

    private TodoTaskBuilder() {
    }

    private TodoTaskBuilder(final TodoTask orginal){
      this.taskId = orginal.getTaskId();
      this.listId = orginal.getListId();
      this.completed = orginal.getCompleted();
      this.description = orginal.getDescription();
    }

    public TodoTaskBuilder taskId(final UUID taskId) {
      this.taskId = taskId;
      return this;
    }

    public TodoTaskBuilder listId(final UUID listId) {
      this.listId = listId;
      return this;
    }

    public TodoTaskBuilder completed(final Boolean completed) {
      this.completed = completed;
      return this;
    }

    public TodoTaskBuilder description(final String description) {
      this.description = description;
      return this;
    }

    public TodoTask build() {
      return new TodoTask(this.taskId, this.listId, this.completed, this.description);
    }
  }
}
