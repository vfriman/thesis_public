package com.todo.core;

import java.util.UUID;

public interface TodoTaskPort {

  /**
   * Adds new todo task into list with completed status false.
   *
   * @param todolistId      Id of the todo list where the task is created
   * @param taskDescription Task description
   * @return Returns UUID of created task
   */
  UUID addTodoTaskToList(final UUID todolistId, final String taskDescription);

  /**
   * Marks task as done.
   *
   * @param todolistId
   * @param taskid
   */
  void markTaskCompleted(final UUID todolistId, final UUID taskid);

}
