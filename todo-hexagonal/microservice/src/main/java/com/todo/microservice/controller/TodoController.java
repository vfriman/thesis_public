package com.todo.microservice.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.todo.core.TodoList;
import com.todo.core.TodoListPort;
import com.todo.core.TodoTaskPort;
import com.todo.core.TodoTask;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@RestController
public class TodoController {
  private final TodoTaskPort todoTaskService;
  private final TodoListPort todoListService;

  public TodoController(final TodoTaskPort todoTaskService, final TodoListPort todoListService) {
    this.todoTaskService = todoTaskService;
    this.todoListService = todoListService;
  }

  @GetMapping("/list")
  public ResponseEntity<List<TodoListDTO>> readLists() {
    return ResponseEntity.ok(this.todoListService.readTodoLists()
      .stream().map(TodoListDTO::from).collect(toList()));
  }

  @PostMapping("/list")
  public ResponseEntity<TodoListCreatedDTO> createTodoList() throws URISyntaxException {
    String listId = this.todoListService.createTodoList().toString();
    return ResponseEntity.created(new URI("/list/"+listId)).body(new TodoListCreatedDTO(listId));
  }

  @GetMapping("/list/{listId}")
  public ResponseEntity<TodoListDTO> readList(@PathVariable final String listId) {
    Optional<TodoList> list = this.todoListService.fetchTodoList(UUID.fromString(listId));
    if (list.isPresent()) {
      return ResponseEntity.ok(TodoListDTO.from(list.get()));
    }
    return ResponseEntity.notFound().build();
  }

  @PostMapping("/list/{listId}")
  public ResponseEntity<Void> createTask(@PathVariable final String listId, @RequestBody final CreateTodoTaskDTO todoTaskDTO) throws URISyntaxException {
    this.todoTaskService.addTodoTaskToList(UUID.fromString(listId), todoTaskDTO.content);
    return ResponseEntity.created(new URI("/list/"+listId)).build();
  }

  @PutMapping("/list/{listId}/task/{taskId}")
  public ResponseEntity<Void> markTaskDone(@PathVariable final String listId, @PathVariable final String taskId) {
    this.todoTaskService.markTaskCompleted(UUID.fromString(listId), UUID.fromString(taskId));

    return ResponseEntity.noContent().build();
  }


  static class TodoListDTO {
    @JsonProperty
    public UUID listId;
    @JsonProperty
    public List<TaskDto> tasks = Collections.emptyList();

    TodoListDTO(){}

    TodoListDTO(final UUID listId, final List<TaskDto> tasks) {
      this.listId = listId;
      this.tasks = tasks;
    }

    @JsonIgnore
    static TodoListDTO from(final TodoList todoList) {
      return new TodoListDTO(todoList.getListId(),
        todoList.getTodoTasks().stream().map(TaskDto::from).collect(toList()));
    }
  }

 static class TaskDto {
    @JsonProperty
    public UUID taskId;
    @JsonProperty
    public Boolean completed;
    @JsonProperty
    public String description;

    TaskDto(final UUID taskId, final Boolean completed, final String description) {
      this.taskId = taskId;
      this.completed = completed;
      this.description = description;
    }

    @JsonIgnore
    static TaskDto from(final TodoTask todoTask) {
      return new TaskDto(todoTask.getTaskId(), todoTask.getCompleted(), todoTask.getDescription());
    }
  }

  static class TodoListCreatedDTO {
    @JsonProperty
    public String listId;

    public TodoListCreatedDTO(final String listId) {
      this.listId = listId;
    }
  }

  static class CreateTodoTaskDTO {
    @JsonProperty
    public String content;

    public CreateTodoTaskDTO() {
    }

    public CreateTodoTaskDTO(final String content) {
      this.content = content;
    }
  }
}
