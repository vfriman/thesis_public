package com.todo.microservice.configuration;


import com.todo.core.TodoListPort;
import com.todo.microservice.repository.InMemoryRepo;
import com.todo.core.TodoPortFactory;
import com.todo.core.TodoTaskPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfiguration {

  @Bean
  public TodoTaskPort todoTaskService() {
    return TodoPortFactory.createTodoTaskService(new InMemoryRepo());
  }

  @Bean
  public TodoListPort todoListService() {
    return TodoPortFactory.createTodoListService(new InMemoryRepo());
  }

}
