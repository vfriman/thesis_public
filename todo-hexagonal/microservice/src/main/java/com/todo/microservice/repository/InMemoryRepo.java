package com.todo.microservice.repository;

import com.todo.core.TodoRepository;
import com.todo.core.TodoList;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class InMemoryRepo implements TodoRepository {

  private static final Map<UUID, TodoList> todoListMap = new ConcurrentHashMap<>();;

  @Override
  public void save(final TodoList todoList) {
    this.todoListMap.put(todoList.getListId(), todoList);
  }

  @Override
  public List<TodoList> read() {
    if(this.todoListMap.isEmpty()){
      return Collections.emptyList();
    }
    return new ArrayList<>(this.todoListMap.values());
  }

  @Override
  public Optional<TodoList> read(final UUID listId) {
    return Optional.ofNullable(this.todoListMap.get(listId));
  }
}
