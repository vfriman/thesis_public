package com.todo.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TodoAppRest {

  public static void main(String... args){
    SpringApplication.run(TodoAppRest.class);
  }
}
