package com.todo.serverless.repository;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.ArrayList;
import java.util.List;

@DynamoDBTable(tableName = "todolist_table")
public class TodoListDynamo {

  @DynamoDBHashKey(attributeName = "todolist_id")
  String todolist_id;

  List<TodoTaskDynamo> tasks = new ArrayList<>();

  public String getTodolist_id() {
    return todolist_id;
  }

  public void setTodolist_id(final String todolist_id) {
    this.todolist_id = todolist_id;
  }

  public List<TodoTaskDynamo> getTasks() {
    return tasks;
  }

  public void setTasks(final List<TodoTaskDynamo> tasks) {
    this.tasks = tasks;
  }
}
