package com.todo.serverless.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.todo.core.TodoListPort;
import com.todo.core.TodoPortFactory;
import com.todo.core.TodoList;
import com.todo.core.TodoRepository;
import com.todo.serverless.repository.DynamoDbRepo;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.amazonaws.HttpMethod.*;

public class TodoListHandler implements
  RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  private final Logger logger = LoggerFactory.getLogger(TodoListHandler.class);

  private final TodoListPort todoListService;

  public TodoListHandler() {
   this(DynamoDbRepo.getInstance());
  }

  TodoListHandler(final TodoRepository todoRepository) {
    this.todoListService = TodoPortFactory.createTodoListService(todoRepository);
  }

  @Override
  public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent requestEvent, final Context context) {
    logger.info("request_event {}", requestEvent);

    if (isPost(requestEvent)) {
      UUID listId = this.todoListService.createTodoList();
      Map<String, String> headers = new HashMap<>();
      headers.put("Location", "/list/" + listId.toString());

      return new APIGatewayProxyResponseEvent()
        .withHeaders(headers)
        .withStatusCode(HttpStatus.SC_CREATED);
    } else if (isGet(requestEvent)) {
      List<TodoListDTO> listdto = new ArrayList<>();
      for (TodoList todoList : this.todoListService.readTodoLists()) {
        listdto.add(TodoListDTO.from(todoList));
      }
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      return new APIGatewayProxyResponseEvent().withStatusCode(200).withBody(gson.toJson(listdto));
    } else {
      return new APIGatewayProxyResponseEvent()
        .withStatusCode(HttpStatus.SC_BAD_REQUEST)
        .withBody(String.format("{\"error_message\": \" Method %s not supported\" }", requestEvent.getHttpMethod()));
    }
  }

  private boolean isPost(final APIGatewayProxyRequestEvent requestEvent) {
    return POST.name().equals(requestEvent.getHttpMethod());
  }

  private boolean isGet(final APIGatewayProxyRequestEvent requestEvent) {
    return GET.name().equals(requestEvent.getHttpMethod());
  }
}
