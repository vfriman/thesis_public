package com.todo.serverless.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.todo.core.TodoPortFactory;
import com.todo.core.TodoRepository;
import com.todo.core.TodoTaskPort;
import com.todo.serverless.repository.DynamoDbRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static com.amazonaws.HttpMethod.POST;
import static com.amazonaws.HttpMethod.PUT;
import static org.apache.http.HttpStatus.*;

public class TodoTaskHandler implements
  RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  private final Logger logger = LoggerFactory.getLogger(TodoListHandler.class);
  private final TodoTaskPort todoTaskService;

  public TodoTaskHandler() {
    this(DynamoDbRepo.getInstance());

  }
  TodoTaskHandler(final TodoRepository repository) {
    this.todoTaskService  = TodoPortFactory.createTodoTaskService(repository);

  }

  @Override
  public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent requestEvent, final Context context) {
    logger.debug("Path parameters {}", requestEvent.getPathParameters());
    String listId = requestEvent.getPathParameters().get("list_id");
    String taskId = requestEvent.getPathParameters().get("task_id");

    if (isPost(requestEvent)) {
      if (listId != null && payloadIsValid(requestEvent.getBody())) { // create new
        JsonObject payload = JsonParser.parseString(requestEvent.getBody()).getAsJsonObject();
        this.todoTaskService.addTodoTaskToList(UUID.fromString(listId), payload.getAsJsonPrimitive("description").getAsString());
        return createResponseWithStatus(SC_CREATED);
      }
      else{
        return createResponseWithStatus(SC_BAD_REQUEST);
      }
    } else if (isPut(requestEvent)) {
      if(listId != null && taskId != null) {
        this.todoTaskService.markTaskCompleted(UUID.fromString(listId), UUID.fromString(taskId));
        return new APIGatewayProxyResponseEvent().withStatusCode(SC_NO_CONTENT);
      }
      else{
        return createResponseWithStatus(SC_BAD_REQUEST);
      }

    }
    return createResponseWithStatus(SC_BAD_REQUEST)
      .withBody(String.format("{\"error_message\": \" Method %s not supported\" }", requestEvent.getHttpMethod()));
  }

  private boolean payloadIsValid(final String body) {
    JsonObject payload = JsonParser.parseString(body).getAsJsonObject();
    return (payload.isJsonObject() && payload.getAsJsonPrimitive("description") != null);

  }

  private APIGatewayProxyResponseEvent createResponseWithStatus(final int status) {
    return new APIGatewayProxyResponseEvent().withStatusCode(status);
  }

  private boolean isPost(final APIGatewayProxyRequestEvent requestEvent) {
    return POST.name().equals(requestEvent.getHttpMethod());
  }

  private boolean isPut(final APIGatewayProxyRequestEvent requestEvent) {
    return PUT.name().equals(requestEvent.getHttpMethod());
  }
}
