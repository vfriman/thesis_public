package com.todo.serverless.repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import com.todo.core.TodoList;
import com.todo.core.TodoTask;
import com.todo.serverless.utils.LocalDynamoDbRule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(LocalDynamoDbRule.class)
class DynamoDbRepoTest {

  private AmazonDynamoDB amazonDynamoDB;
  private DynamoDbRepo repository;

  DynamoDbRepoTest(final AmazonDynamoDB amazonDynamoDB) {
    this.amazonDynamoDB = amazonDynamoDB;
  }

  @BeforeEach
  public void setUp() {
    this.repository = new DynamoDbRepo(amazonDynamoDB);
    var dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);
    try {
      CreateTableRequest tableRequest = dynamoDBMapper.generateCreateTableRequest(TodoListDynamo.class);
      tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
      amazonDynamoDB.createTable(tableRequest);
    } catch (ResourceInUseException e) {
      // Do nothing, table already created
    }
  }
  @AfterEach
  public void tearDown(){
    DynamoDBMapper mapper = new DynamoDBMapper(amazonDynamoDB);
    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
    mapper.batchDelete( mapper.scan(TodoListDynamo.class, scanExpression));
  }

  @Test
  public void shouldSaveAndRetrieveTodoList() {
    var task = TodoTask.builder()
      .taskId(UUID.randomUUID()).description("test task").completed(false).build();
    var todoList = TodoList.builder().listId(UUID.randomUUID()).todoTasks(List.of(task)).build();
    this.repository.save(todoList);

    var savedLists = this.repository.read();

    assertEquals(1, savedLists.size(), "There should be only one list");
    var savedList = savedLists.get(0);
    assertEquals(1, savedList.getTodoTasks().size(), "Saved list should have one task");
    var savedTask = savedList.getTodoTasks().get(0);
    assertEquals(task.getTaskId(), savedTask.getTaskId(), "Task id should match");
    assertEquals(task.getDescription(), savedTask.getDescription(), "Task description should match");
    assertFalse(task.getCompleted(), "Task should be marked as non completed");
  }

  @Test
  public void shouldSaveDoneTaskCorrectly() {
    var task = TodoTask.builder()
      .taskId(UUID.randomUUID()).description("test task").completed(false).build();
    var todoList = TodoList.builder().listId(UUID.randomUUID()).todoTasks(List.of(task)).build();
    this.repository.save(todoList);

    var savedList = this.repository.read(todoList.getListId()).get();
    var completedTask = TodoTask.builder(savedList.getTodoTasks().get(0)).completed(true).build();
    var updatedList = TodoList.builder().listId(savedList.getListId())
      .todoTasks(List.of(completedTask)).build();
    this.repository.save(updatedList);

    var completedList = this.repository.read(todoList.getListId()).get();
    assertTrue(completedList.getTodoTasks().get(0).getCompleted(), "Task should be marked as done");

  }

}
