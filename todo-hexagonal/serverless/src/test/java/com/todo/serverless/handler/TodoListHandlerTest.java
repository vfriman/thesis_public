package com.todo.serverless.handler;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.todo.core.TodoList;
import com.todo.core.TodoRepository;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;

class TodoListHandlerTest {

  private static final Context NULL_CONTEXT = null;
  private TodoRepository repository;
  private TodoListHandler handler;

  @BeforeEach
  void setUp() {
    this.repository = new StubTodoRepository();
    this.handler = new TodoListHandler(this.repository);
  }

  @Test
  void shouldCreateNewTodoList(){
    var responseEvent = this.handler.handleRequest(createPostEvent(), NULL_CONTEXT);

    assertEquals(responseEvent.getStatusCode().intValue(), HttpStatus.SC_CREATED);
    assertNotNull(responseEvent.getHeaders().get("Location"), "Event should have location header");

  }
  @Test
  void shouldReturnTodoLists(){
    this.repository.save(TodoList.builder().listId(UUID.randomUUID()).build());

    var responseEvent = this.handler.handleRequest(createGetEvent(), NULL_CONTEXT);

    assertEquals(responseEvent.getStatusCode().intValue(), HttpStatus.SC_OK);
    assertFalse(responseEvent.getBody().isEmpty());
  }

  @Test
  void unsupportedMethodsReturn400() {
    var unsupportedMethods = Arrays.stream(HttpMethod.values())
      .filter(method -> !(method.equals(HttpMethod.GET) || method.equals(HttpMethod.POST)))
      .collect(toList());

    unsupportedMethods.stream().forEach(method ->{
      var responseEvent = this.handler.handleRequest(createEvent(method), NULL_CONTEXT);
      assertEquals(responseEvent.getStatusCode().intValue(), HttpStatus.SC_BAD_REQUEST,
        "Expected method "+method.name()+" to return status 400");
    } );
  }

  private APIGatewayProxyRequestEvent createPostEvent() {
    return createEvent(HttpMethod.POST);
  }
  private APIGatewayProxyRequestEvent createGetEvent() {
    return createEvent(HttpMethod.GET);
  }

  private APIGatewayProxyRequestEvent createEvent(final HttpMethod method) {
    var event = new APIGatewayProxyRequestEvent();
    event.setHttpMethod(method.name());
    return event;
  }


}
