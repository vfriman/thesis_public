package com.todo.serverless.handler;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.todo.core.TodoList;
import com.todo.core.TodoRepository;
import com.todo.core.TodoTask;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TodoTaskHandlerTest {

  private static final Context NULL_CONTEXT = null;
  private TodoRepository repository;
  private TodoTaskHandler handler;

  @BeforeEach
  void setUp() {
    this.repository = new StubTodoRepository();
    this.handler = new TodoTaskHandler(this.repository);
  }

  @Test
  void shouldCreateNewTask() {
    UUID listId = createEmptyList();
    var event = createPostEvent();
    var pathParams = new HashMap<String, String>();
    pathParams.put("list_id", listId.toString());
    event.setPathParameters(pathParams);
    event.setBody("{\"description\":\"test\"}");

    var response = handler.handleRequest(event, NULL_CONTEXT);

    assertEquals(response.getStatusCode().intValue(), HttpStatus.SC_CREATED);
  }

  @Test
  void shouldMarkTaskDone() {
    UUID listId = createEmptyList();
    UUID taskId = addTaskToList(listId);

    var event = createPutEvent();
    var pathParams = new HashMap<String, String>();
    pathParams.put("list_id", listId.toString());
    pathParams.put("task_id", taskId.toString());
    event.setPathParameters(pathParams);

    var response = handler.handleRequest(event, NULL_CONTEXT);

    assertEquals(response.getStatusCode().intValue(), HttpStatus.SC_NO_CONTENT);
  }

  @Test
  void unsupportedMethodsReturn400() {
    final var unsupportedMethods = Arrays.stream(HttpMethod.values())
      .filter(method -> !(method.equals(HttpMethod.PUT) || method.equals(HttpMethod.POST)))
      .collect(toList());
    final var params = new HashMap<String, String>();

    unsupportedMethods.stream().forEach(method ->{
      var event = createEvent(method);
      event.setPathParameters(params);
      var responseEvent = this.handler.handleRequest(event, NULL_CONTEXT);
      assertEquals(responseEvent.getStatusCode().intValue(), HttpStatus.SC_BAD_REQUEST,
        "Expected method "+method.name()+" to return status 400");
    } );
  }

  private APIGatewayProxyRequestEvent createPostEvent() {
    return createEvent(HttpMethod.POST);
  }
  private APIGatewayProxyRequestEvent createPutEvent() {
    return createEvent(HttpMethod.PUT);
  }

  private APIGatewayProxyRequestEvent createEvent(final HttpMethod method) {
    var event = new APIGatewayProxyRequestEvent();
    event.setHttpMethod(method.name());
    return event;
  }

  private UUID createEmptyList() {
    TodoList todoList = TodoList.builder().listId(UUID.randomUUID()).build();
    this.repository.save(todoList);
    return todoList.getListId();
  }

  private UUID addTaskToList(final UUID listId) {
    TodoTask task = TodoTask.builder().taskId(UUID.randomUUID()).build();
    TodoList list = this.repository.read(listId).get();
    TodoList updatedList = TodoList.builder().listId(list.getListId())
      .todoTasks(List.of(task)).build();
    this.repository.save(updatedList);

    return task.getTaskId();
  }

}
