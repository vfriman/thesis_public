package com.todo.serverless.utils;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import org.junit.jupiter.api.extension.*;
import org.junit.platform.commons.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Properties;

/**
 * Based on https://www.baeldung.com/dynamodb-local-integration-tests
 */
public class LocalDynamoDbRule implements BeforeAllCallback, AfterAllCallback, ParameterResolver {

  private static final String DYNAMODB_ENDPOINT = "amazon.dynamodb.endpoint";
  private static final String AWS_ACCESSKEY = "amazon.aws.accesskey";
  private static final String AWS_SECRETKEY = "amazon.aws.secretkey";

  private String serverPort;
  private DynamoDBProxyServer server;

  public LocalDynamoDbRule() {
    System.setProperty("sqlite4java.library.path", "native-libs");
  }

  @Override
  public void afterAll(final ExtensionContext extensionContext) throws Exception {
    this.stopUnchecked(server);
  }

  @Override
  public void beforeAll(final ExtensionContext extensionContext) throws Exception {
    this.serverPort = getAvailablePort();
    server = ServerRunner.createServerFromCommandLineArgs(
      new String[]{"-inMemory", "-port", serverPort});
    server.start();
  }


  private void stopUnchecked(DynamoDBProxyServer dynamoDbServer) {
    try {
      dynamoDbServer.stop();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private String getAvailablePort() throws IOException {
    if(this.serverPort != null){
      return this.serverPort;
    }
    ServerSocket serverSocket = new ServerSocket(0);
    this.serverPort = String.valueOf(serverSocket.getLocalPort());
    return this.serverPort;
  }

  @Override
  public boolean supportsParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext) throws ParameterResolutionException {
    return parameterContext.getParameter().getType()
      .equals(AmazonDynamoDB.class);
  }

  @Override
  public Object resolveParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext) throws ParameterResolutionException {
    var testProperties = loadFromFileInClasspath("test.properties")
      .filter(properties -> !StringUtils.isBlank(properties.getProperty(AWS_ACCESSKEY)))
      .filter(properties -> !StringUtils.isBlank(properties.getProperty(AWS_SECRETKEY)))
      .filter(properties -> !StringUtils.isBlank(properties.getProperty(DYNAMODB_ENDPOINT)))
      .orElseThrow(() -> new RuntimeException("Unable to get all of the required test property values"));

    try {
      this.serverPort = getAvailablePort();
    } catch (IOException e) {
      throw new RuntimeException("Unable to get available port for DynamoDB instance ",e);
    }
    var amazonAWSAccessKey = testProperties.getProperty(AWS_ACCESSKEY);
    var amazonAWSSecretKey = testProperties.getProperty(AWS_SECRETKEY);
    var amazonDynamoDBEndpoint = String.format(testProperties.getProperty(DYNAMODB_ENDPOINT), this.serverPort);

    var credentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey));
    var endpointConfig = new AwsClientBuilder.EndpointConfiguration(amazonDynamoDBEndpoint, null);
   return AmazonDynamoDBClient.builder()
      .withCredentials(credentialsProvider)
      .withEndpointConfiguration(endpointConfig)
      .build();
  }


  private static Optional<Properties> loadFromFileInClasspath(final String fileName) {
    InputStream stream = null;
    try {
      Properties config = new Properties();
      Path configLocation = Paths.get(ClassLoader.getSystemResource(fileName).toURI());
      stream = Files.newInputStream(configLocation);
      config.load(stream);
      return Optional.of(config);
    } catch (Exception e) {
      return Optional.empty();
    } finally {
      if (stream != null) {
        try {
          stream.close();
        } catch (IOException e) {
        }
      }
    }
  }
}
