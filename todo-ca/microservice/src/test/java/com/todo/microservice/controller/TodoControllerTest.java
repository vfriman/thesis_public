package com.todo.microservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void createTodoList() throws Exception {

    this.mockMvc.perform(post("/list"))
      .andExpect(status().isCreated())
      .andExpect(header().exists("Location"))
      .andExpect(jsonPath("$.listId").isString());
  }

  @Test
  void shouldReturnAllTheTodoLists() throws Exception {
    this.mockMvc.perform(post("/list"))
      .andExpect(status().isCreated());
    this.mockMvc.perform(post("/list"))
      .andExpect(status().isCreated());

    this.mockMvc.perform(get("/list"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isNotEmpty());
  }

  @Test
  void shouldReturn404WhenListNotFound() throws Exception {
    this.mockMvc.perform(get("/list/"+UUID.randomUUID()))
      .andExpect(status().isNotFound());
  }

  @Test
  void createTaskToList() throws Exception {

    UUID listId = createTodolist();

    this.mockMvc.perform(post("/list/"+listId.toString())
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectToString(new TodoController.CreateTodoTaskDTO("test_task"))))
      .andExpect(status().isCreated())
      .andExpect(header().exists("Location"));
  }

  private UUID createTodolist() throws Exception {
    var resultAction = this.mockMvc.perform(post("/list"))
       .andExpect(status().isCreated());

    var content = resultAction.andReturn().getResponse().getContentAsString();
    var todoList = this.objectMapper.readValue(content, TodoController.TodoListDTO.class);
    return todoList.listId;
  }

  private String objectToString( final Object payload) throws JsonProcessingException {
    var tmp = this.objectMapper.writeValueAsString(payload);

    return tmp;
  }
}
