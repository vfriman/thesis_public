package com.todo.microservice.configuration;

import com.todo.core.repository.TodoRepository;
import com.todo.microservice.repository.InMemoryRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReposítoryConfiguration {

  @Bean
  TodoRepository todoRepository() {
    return new InMemoryRepo();
  }
}
