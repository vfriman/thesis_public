package com.todo.microservice.configuration;


import com.todo.core.repository.TodoRepository;
import com.todo.core.usecase.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfiguration {

  @Autowired
  TodoRepository todoRepository;


  @Bean
  public Usecase<AddTaskRequest, AddTaskResponse> addTask() {
    return TodoUsecaseFactory.addTask(todoRepository);
  }

  @Bean
  public Usecase<VoidRequest, CreateTodoListResponse> createTodoList() {
    return TodoUsecaseFactory.createTodoList(todoRepository);
  }

  @Bean
  public Usecase<FindTodoListRequest, FindTodoListResponse> findTodoList() {
    return TodoUsecaseFactory.findTodoList(todoRepository);
  }

  @Bean
  public Usecase<VoidRequest, FindTodoListsResponse> findTodoLists() {
    return TodoUsecaseFactory.findTodoLists(todoRepository);
  }

  @Bean
  public Usecase<MarkTaskDoneRequest, MarkTaskDoneResponse> markTaskDone() {
    return TodoUsecaseFactory.markTaskDone(todoRepository);
  }
}
