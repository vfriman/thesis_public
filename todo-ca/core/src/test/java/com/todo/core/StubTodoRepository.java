package com.todo.core;

import com.todo.core.domain.TodoList;
import com.todo.core.repository.TodoRepository;

import java.util.*;

/**
 * Stub implementation of the TodoRepository interface
 * for unit tests.
 */
class StubTodoRepository implements TodoRepository {

  private static final Map<UUID, TodoList> todoLists = new HashMap<>();

  @Override
  public void save(final TodoList todoList) {
    this.todoLists.put(todoList.getListId(), todoList);
  }

  @Override
  public List<TodoList> read() {
    return List.copyOf(this.todoLists.values());
  }

  @Override
  public Optional<TodoList> read(final UUID listId) {
    return Optional.ofNullable(this.todoLists.get(listId));
  }
}
