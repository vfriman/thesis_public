package com.todo.core.usecase;

import java.util.Objects;
import java.util.UUID;

public final class FindTodoListRequest implements Request {

  private final UUID listId;

  public FindTodoListRequest(final UUID listId) {
    this.listId = Objects.requireNonNull(listId, "List id cannot be null");
  }

  UUID getListId() {
    return listId;
  }

  @Override
  public String toString() {
    return "FindTodoListRequest{" +
      "listId=" + listId +
      '}';
  }
}
