package com.todo.core.usecase;

import java.util.Objects;
import java.util.UUID;

public final class MarkTaskDoneRequest implements Request{

  private final UUID listId;
  private final UUID taskId;

  private MarkTaskDoneRequest(final UUID listId, final UUID taskId) {
    this.listId = Objects.requireNonNull(listId, "List id cannot be null");
    this.taskId = Objects.requireNonNull(taskId, "Task id cannot be null");
  }

  UUID getListId() {
    return listId;
  }

  UUID getTaskId() {
    return taskId;
  }

  @Override
  public String toString() {
    return "MarkTaskDoneRequest{" +
      "listId=" + listId +
      ", taskId=" + taskId +
      '}';
  }

  public static MarkTaskDoneRequestBuilder builder() {
    return new MarkTaskDoneRequestBuilder();
  }

  public static class MarkTaskDoneRequestBuilder{
    private UUID listId;
    private UUID taskId;

    private MarkTaskDoneRequestBuilder() {}

    public MarkTaskDoneRequestBuilder listId(final UUID listId) {
      this.listId = listId;
      return this;

    }
    public MarkTaskDoneRequestBuilder taskId(final UUID taskId) {
      this.taskId = taskId;
      return this;
    }

    public MarkTaskDoneRequest build() {
      return  new MarkTaskDoneRequest(listId, taskId);
    }
  }
}
