package com.todo.core.usecase;

import java.util.Optional;

public abstract class Response<T> {

  private final Throwable throwable;

  Response(final Throwable throwable) {
    this.throwable = throwable;
  }

  public abstract Optional<T> result();

  public final Optional<? extends Throwable> failure() {
    return Optional.ofNullable(throwable);
  }
}

