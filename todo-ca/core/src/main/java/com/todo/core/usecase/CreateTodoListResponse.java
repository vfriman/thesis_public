package com.todo.core.usecase;

import java.util.Optional;
import java.util.UUID;

public final class CreateTodoListResponse extends Response<UUID> {

  private final UUID listId;

  CreateTodoListResponse(final UUID listId) {
    super(null);
    this.listId = listId;
  }

  public Optional<UUID> result() {
    return Optional.ofNullable(listId);
  }

  @Override
  public String toString() {
    return "CreateNewTodoListResponse{" +
      "listId=" + listId +
      '}';
  }
}
