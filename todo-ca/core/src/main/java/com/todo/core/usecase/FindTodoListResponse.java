package com.todo.core.usecase;

import com.todo.core.domain.TodoList;

import java.util.Optional;

public class FindTodoListResponse extends Response<TodoList>{

  private final TodoList todoList;

  FindTodoListResponse(final TodoList todoList) {
    super(null);
    this.todoList = todoList;
  }

  @Override
  public Optional<TodoList> result() {
    return Optional.ofNullable(todoList);
  }

  @Override
  public String toString() {
    return "FindTodoListResponse{" +
      "optionalTodoList=" + todoList+
      "Failure=" + super.failure() +
      '}';
  }
}
