package com.todo.core.usecase;

import java.util.Objects;
import java.util.UUID;

public final class AddTaskRequest implements Request{
  private final UUID listId;
  private final String description;

  public AddTaskRequest(final UUID listId, final String description) {
    this.listId = Objects.requireNonNull(listId, "list id cannot be null");
    this.description = Objects.requireNonNull(description, "Description cannot be null");
  }

  UUID getListId() {
    return listId;
  }

  String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return "AddTaskRequest{" +
      "listId=" + listId +
      ", description='" + description + '\'' +
      '}';
  }
}
