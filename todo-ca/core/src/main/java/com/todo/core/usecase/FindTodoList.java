package com.todo.core.usecase;

import com.todo.core.repository.TodoRepository;

final class FindTodoList implements Usecase<FindTodoListRequest, FindTodoListResponse>{

  private final TodoRepository repository;

  FindTodoList(final TodoRepository repository) {
    this.repository = repository;
  }


  @Override
  public FindTodoListResponse  handle(final FindTodoListRequest request) {
    return new FindTodoListResponse(this.repository.read(request.getListId()).orElse(null));
  }
}
