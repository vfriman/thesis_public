package com.todo.core.domain;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * TodoList data transfer object
 */
public final class TodoList {

  private final UUID listId;
  private List<TodoTask> todoTasks;

  private TodoList(final UUID listId, final List<TodoTask> todoTasks) {
    this.listId = listId;
    this.todoTasks = todoTasks;
  }

  /**
   * Returns List's identifier
   *
   * @return
   */
  public UUID getListId() {
    return listId;
  }

  /**
   * Returns list's tasks. Returns empty list if there are no tasks.
   *
   * @return
   */
  public List<TodoTask> getTodoTasks() {
    return List.copyOf(todoTasks);
  }

  @Override
  public String toString() {
    return "TodoList{" +
      "listId=" + listId +
      ", todoTasks=" + todoTasks +
      '}';
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    final TodoList todoList = (TodoList) o;
    return Objects.equals(listId, todoList.listId) &&
      Objects.equals(todoTasks, todoList.todoTasks);
  }

  @Override
  public int hashCode() {
    return Objects.hash(listId, todoTasks);
  }

  public static TodoListBuilder builder() {
    return new TodoListBuilder();
  }

  public static class TodoListBuilder {
    private UUID listId;
    private List<TodoTask> todoTasks;

    private TodoListBuilder() {
    }

    public TodoListBuilder listId(final UUID listId) {
      this.listId = listId;
      return this;
    }

    public TodoListBuilder todoTasks(final List<TodoTask> todoTasks) {
      this.todoTasks = todoTasks == null ? Collections.emptyList() : List.copyOf(todoTasks);
      return this;
    }

    public TodoList build() {
      if(todoTasks == null) {
        this.todoTasks = Collections.emptyList();
      }
      Objects.requireNonNull(this.listId, "List id cannot be null");
      return new TodoList(this.listId, this.todoTasks);
    }
  }
}
