package com.todo.core.usecase;

import java.util.Optional;

public final class MarkTaskDoneResponse extends Response<Boolean>{

  private final boolean success;

  MarkTaskDoneResponse(final boolean success) {
    super(null);
    this.success = success;
  }

  public boolean isSuccess() {
    return success;
  }

  @Override
  public String toString() {
    return "MarkTaskDoneResponse{" +
      "success=" + success +
      '}';
  }

  @Override
  public Optional<Boolean> result() {
    return Optional.ofNullable(this.success);
  }
}
