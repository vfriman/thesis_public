package com.todo.core.usecase;

/**
 * Interface implemented by all use cases
 *
 * @param <T> Type of request object
 * @param <E> Type of response object
 */
public interface Usecase<T extends Request, E extends Response> {

  E handle(T t);
}
