package com.todo.core.usecase;

import com.todo.core.domain.TodoList;
import com.todo.core.domain.TodoTask;
import com.todo.core.repository.TodoRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static java.lang.Boolean.FALSE;

final class AddTask implements Usecase<AddTaskRequest, AddTaskResponse>{


  private final TodoRepository repository;
  private final Usecase<FindTodoListRequest, FindTodoListResponse> findTodoList;

  AddTask(final TodoRepository repository, final Usecase<FindTodoListRequest, FindTodoListResponse> findTodoList) {
    this.repository = repository;
    this.findTodoList = findTodoList;
  }

  @Override
  public AddTaskResponse handle(final AddTaskRequest request) {
    var response = this.findTodoList.handle(new FindTodoListRequest(request.getListId()));
    if (response.result().isEmpty()) {
      return new AddTaskResponse(null);
    }
    TodoList todoList = (TodoList) response.result().get();
    var todoTask = TodoTask.builder()
      .listId(request.getListId())
      .taskId(UUID.randomUUID())
      .completed(FALSE)
      .description(request.getDescription())
      .build();

    var updatedTasks = new ArrayList<>(todoList.getTodoTasks());
    updatedTasks.add(todoTask);
    var updatedTodoList = TodoList.builder()
      .listId(todoList.getListId()).todoTasks(updatedTasks).build();

    this.repository.save(updatedTodoList);

    return new AddTaskResponse(todoTask.getTaskId());
  }
}
