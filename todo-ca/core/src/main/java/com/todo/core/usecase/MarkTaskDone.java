package com.todo.core.usecase;

import com.todo.core.domain.TodoList;
import com.todo.core.domain.TodoTask;
import com.todo.core.repository.TodoRepository;

import static java.lang.Boolean.TRUE;
import static java.util.stream.Collectors.toList;

final class MarkTaskDone implements Usecase<MarkTaskDoneRequest, MarkTaskDoneResponse>{
  private final TodoRepository todoRepository;

  MarkTaskDone(final TodoRepository todoRepository) {
    this.todoRepository = todoRepository;
  }


  @Override
  public MarkTaskDoneResponse handle(final MarkTaskDoneRequest request) {
    var optionalList = this.todoRepository.read(request.getListId());

    if(optionalList.isEmpty()) {
      return new MarkTaskDoneResponse(false);
    }
    var todoList = optionalList.get();
    var markedToBeDoneTask = todoList.getTodoTasks().stream()
      .filter(task -> task.getTaskId().equals(request.getTaskId())).findFirst().get();
    var updatedTask = TodoTask.builder(markedToBeDoneTask).completed(TRUE).build();
    var updatedTasks = todoList.getTodoTasks().stream()
      .filter(task -> !task.getTaskId().equals(request.getTaskId()))
      .collect(toList());
    updatedTasks.add(updatedTask);

    var updatedList = TodoList.builder().listId(todoList.getListId()).todoTasks(updatedTasks).build();

    this.todoRepository.save(updatedList);
    return new MarkTaskDoneResponse(true);
  }
}
