package com.todo.core.usecase;

import com.todo.core.domain.TodoList;
import com.todo.core.repository.TodoRepository;

import java.util.UUID;

final class CreateTodoList implements Usecase<VoidRequest, CreateTodoListResponse>{

  private final TodoRepository repository;

  CreateTodoList(final TodoRepository repository) {
    this.repository = repository;
  }

  @Override
  public CreateTodoListResponse handle(final VoidRequest voidRequest) {
    var todoList = TodoList.builder().listId(UUID.randomUUID()).build();
    this.repository.save(todoList);

    return new CreateTodoListResponse(todoList.getListId());
  }
}
