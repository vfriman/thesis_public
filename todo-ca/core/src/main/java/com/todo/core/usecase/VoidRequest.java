package com.todo.core.usecase;

public final class VoidRequest implements Request{

  private static final VoidRequest INSTANCE = new VoidRequest();
  private VoidRequest(){};

  public static VoidRequest getInstance(){return INSTANCE;}
}
