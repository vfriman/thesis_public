package com.todo.core.usecase;

import com.todo.core.domain.TodoList;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class FindTodoListsResponse extends Response<List<TodoList>>{

  private final List<TodoList> list;

  FindTodoListsResponse(final List<TodoList> list) {
    super(null);
    this.list = list == null ? Collections.emptyList() : List.copyOf(list);
  }

  @Override
  public Optional<List<TodoList>> result() {
    return Optional.of(list);
  }

  @Override
  public String toString() {
    return "FindTodoListsResponse{" +
      "list=" + list +
      '}';
  }


}
