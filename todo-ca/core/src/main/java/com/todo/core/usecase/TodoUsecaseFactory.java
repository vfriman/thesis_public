package com.todo.core.usecase;

import com.todo.core.repository.TodoRepository;

import java.util.Objects;

/**
 * A factory class which knows how to assemble use cases
 */
public class TodoUsecaseFactory {

  public static Usecase<AddTaskRequest, AddTaskResponse> addTask(final TodoRepository todoRepository) {
    checkForNull(todoRepository);
    return new AddTask(todoRepository, findTodoList(todoRepository));
  }

  public static Usecase<VoidRequest, CreateTodoListResponse> createTodoList(final TodoRepository todoRepository) {
    checkForNull(todoRepository);
    return new CreateTodoList(todoRepository);
  }

  public static Usecase<FindTodoListRequest, FindTodoListResponse> findTodoList(final TodoRepository todoRepository) {
    checkForNull(todoRepository);
    return new FindTodoList(todoRepository);
  }

  public static Usecase<VoidRequest, FindTodoListsResponse> findTodoLists(final TodoRepository todoRepository) {
    checkForNull(todoRepository);
    return new FindTodoLists(todoRepository);
  }

  public static Usecase<MarkTaskDoneRequest, MarkTaskDoneResponse> markTaskDone(final TodoRepository todoRepository) {
    checkForNull(todoRepository);
    return new MarkTaskDone(todoRepository);
  }

  private static void checkForNull(final TodoRepository todoRepository) {
    Objects.requireNonNull(todoRepository, "Repository implementation cannot be null");
  }

}
