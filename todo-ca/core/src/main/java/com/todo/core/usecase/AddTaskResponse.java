package com.todo.core.usecase;

import java.util.Optional;
import java.util.UUID;

public final class AddTaskResponse  extends Response<UUID>{

  private final Optional<UUID> taskId;

  AddTaskResponse(final UUID taskId) {
    super(null);
    this.taskId = Optional.ofNullable(taskId);
  }



  @Override
  public String toString() {
    return "AddTaskResponse{" +
      "taskId=" + taskId +
      '}';
  }

  @Override
  public Optional<UUID> result() {
    return this.taskId;
  }
}
