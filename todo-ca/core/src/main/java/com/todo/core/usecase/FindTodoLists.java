package com.todo.core.usecase;

import com.todo.core.repository.TodoRepository;

final class FindTodoLists implements Usecase<VoidRequest, FindTodoListsResponse>{

  private final TodoRepository repository;

  FindTodoLists(final TodoRepository repository) {
    this.repository = repository;
  }

  @Override
  public FindTodoListsResponse handle(final VoidRequest voidRequest) {
    return new FindTodoListsResponse(this.repository.read());
  }
}
