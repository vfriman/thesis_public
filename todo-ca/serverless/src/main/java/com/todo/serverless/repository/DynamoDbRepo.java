package com.todo.serverless.repository;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.todo.core.repository.TodoRepository;
import com.todo.core.domain.TodoList;
import com.todo.core.domain.TodoTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class DynamoDbRepo implements TodoRepository {

  private Logger logger = LoggerFactory.getLogger(DynamoDbRepo.class);
  private final static DynamoDbRepo adapter = new DynamoDbRepo();

  private final AmazonDynamoDB client;

  private DynamoDbRepo() {
    client = AmazonDynamoDBClientBuilder.standard()
      .withRegion(Regions.EU_CENTRAL_1)
      .build();
    logger.info("Created DynamoDB client");
  }

  DynamoDbRepo(final AmazonDynamoDB dynamoDB) {
    this.client = dynamoDB;
  }

  public static TodoRepository getInstance() {
    return adapter;
  }

  @Override
  public void save(final TodoList todoList) {
    DynamoDBMapper mapper = new DynamoDBMapper(client);
    mapper.save(this.toTodoListDynamo(todoList));
  }

  @Override
  public List<TodoList> read() {

    DynamoDBMapper mapper = new DynamoDBMapper(client);
    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
    scanExpression.setLimit(100);
    List<TodoListDynamo> todoListDynamo = mapper.scan(TodoListDynamo.class, scanExpression);
    return todoListDynamo.stream().map(this::fromDynamoList).collect(Collectors.toList());
  }

  @Override
  public Optional<TodoList> read(final UUID listId) {
    DynamoDBMapper mapper = new DynamoDBMapper(client);
    Map<String, AttributeValue> vals = new HashMap<>();
    vals.put(":todolist_id", new AttributeValue().withS(listId.toString()));

    DynamoDBQueryExpression<TodoListDynamo> queryExpression = new DynamoDBQueryExpression<TodoListDynamo>()
      .withKeyConditionExpression("todolist_id = :todolist_id ")
      .withExpressionAttributeValues(vals);

    List<TodoListDynamo> todoListDynamo = mapper.query(TodoListDynamo.class, queryExpression);
    if (todoListDynamo.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(this.fromDynamoList(todoListDynamo.get(0)));
  }

  private TodoList fromDynamoList(final TodoListDynamo todoListDynamo) {
    final UUID listId = UUID.fromString(todoListDynamo.todolist_id);
    List<TodoTask> todoTaskList = todoListDynamo.tasks.stream()
      .map(task -> fromDynamoTask(listId, task))
      .collect(Collectors.toList());
    return TodoList.builder().listId(listId).todoTasks(todoTaskList).build();
  }

  private TodoTask fromDynamoTask(final UUID listId, final TodoTaskDynamo todoTaskDynamo) {
    return TodoTask.builder().taskId(UUID.fromString(todoTaskDynamo.task_id))
      .listId(listId).completed(todoTaskDynamo.completed).description(todoTaskDynamo.description)
      .build();
  }

  private TodoListDynamo toTodoListDynamo(final TodoList todoList) {

    List<TodoTaskDynamo> tasks = todoList.getTodoTasks().stream()
      .map(this::toTodoTaskDynamo).collect(Collectors.toList());
    TodoListDynamo todoListDynamo = new TodoListDynamo();
    todoListDynamo.todolist_id = todoList.getListId().toString();
    todoListDynamo.tasks = tasks;

    return todoListDynamo;
  }

  private TodoTaskDynamo toTodoTaskDynamo(final TodoTask todoTask) {
    TodoTaskDynamo todoTaskDynamo = new TodoTaskDynamo();
    todoTaskDynamo.task_id = todoTask.getTaskId().toString();
    todoTaskDynamo.completed = todoTask.getCompleted();
    todoTaskDynamo.description = todoTask.getDescription();
    return todoTaskDynamo;
  }
}
