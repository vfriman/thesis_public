package com.todo.serverless.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class TodoTaskDynamo {

  @DynamoDBAttribute(attributeName = "task_id")
  String task_id;
  @DynamoDBAttribute(attributeName = "completed")
  Boolean completed;
  @DynamoDBAttribute(attributeName = "description")
  String description;

  public String getTask_id() {
    return task_id;
  }

  public void setTask_id(final String task_id) {
    this.task_id = task_id;
  }

  public Boolean getCompleted() {
    return completed;
  }

  public void setCompleted(final Boolean completed) {
    this.completed = completed;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }
}
