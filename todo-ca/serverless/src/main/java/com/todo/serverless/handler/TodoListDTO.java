package com.todo.serverless.handler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.todo.core.domain.TodoList;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

class TodoListDTO {
  @JsonProperty
  public UUID list_id;
  @JsonProperty
  public List<TaskDto> tasks;

  TodoListDTO(final UUID listId, final List<TaskDto> tasks) {
    this.list_id = listId;
    this.tasks = tasks;
  }

  @JsonIgnore
  static TodoListDTO from(final TodoList todoList) {
    return new TodoListDTO(todoList.getListId(),
      todoList.getTodoTasks().stream().map(TaskDto::from).collect(toList()));
  }
}
