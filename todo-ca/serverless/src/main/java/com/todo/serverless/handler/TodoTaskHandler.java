package com.todo.serverless.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.todo.core.repository.TodoRepository;
import com.todo.core.usecase.*;
import com.todo.serverless.repository.DynamoDbRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static com.amazonaws.HttpMethod.POST;
import static com.amazonaws.HttpMethod.PUT;
import static org.apache.http.HttpStatus.*;

public class TodoTaskHandler implements
  RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  private final Logger logger = LoggerFactory.getLogger(TodoListHandler.class);
  private final Usecase<AddTaskRequest, AddTaskResponse> addTask;
  private final Usecase<MarkTaskDoneRequest, MarkTaskDoneResponse> markTaskDone;

  public TodoTaskHandler() {
    this(DynamoDbRepo.getInstance());

  }
  TodoTaskHandler(final TodoRepository repository) {
    this.addTask = TodoUsecaseFactory.addTask(repository);
    this.markTaskDone = TodoUsecaseFactory.markTaskDone(repository);
  }

  @Override
  public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent requestEvent, final Context context) {
    logger.debug("Path parameters {}", requestEvent.getPathParameters());
    String listId = requestEvent.getPathParameters().get("list_id");
    String taskId = requestEvent.getPathParameters().get("task_id");

    if(listId == null){
      return createResponseWithStatus(SC_BAD_REQUEST)
        .withBody(String.format("{\"error_message\": \"list id cannot be null\" }"));
    }

    if (isPost(requestEvent)) {
      if (payloadIsValid(requestEvent.getBody())) { // create new
        JsonObject payload = JsonParser.parseString(requestEvent.getBody()).getAsJsonObject();
        String description = payload.getAsJsonPrimitive("description").getAsString();
        var request = new AddTaskRequest(UUID.fromString(listId),description);
        var response = this.addTask.handle(request);
        if(response.result().isPresent()) {
          this.logger.debug("Created task {} to list {}", response.result().get(), listId);
          return createResponseWithStatus(SC_CREATED);
        }
        return createResponseWithStatus(SC_INTERNAL_SERVER_ERROR);
      }
      else{
        return createResponseWithStatus(SC_BAD_REQUEST);
      }
    } else if (isPut(requestEvent)) {
      if(taskId != null) {
        var request = MarkTaskDoneRequest.builder()
          .listId(UUID.fromString(listId))
          .taskId(UUID.fromString(taskId))
          .build();
        var response = this.markTaskDone.handle(request);
        if(response.isSuccess()) {
          return createResponseWithStatus(SC_NO_CONTENT);
        }
        return createResponseWithStatus(SC_INTERNAL_SERVER_ERROR);
      }
      else{
        return createResponseWithStatus(SC_BAD_REQUEST);
      }

    }
    return createResponseWithStatus(SC_BAD_REQUEST)
      .withBody(String.format("{\"error_message\": \" Method %s not supported\" }", requestEvent.getHttpMethod()));
  }

  private boolean payloadIsValid(final String body) {
    JsonObject payload = JsonParser.parseString(body).getAsJsonObject();
    return (payload.isJsonObject() && payload.getAsJsonPrimitive("description") != null);

  }

  private APIGatewayProxyResponseEvent createResponseWithStatus(final int status) {
    return new APIGatewayProxyResponseEvent().withStatusCode(status);
  }

  private boolean isPost(final APIGatewayProxyRequestEvent requestEvent) {
    return POST.name().equals(requestEvent.getHttpMethod());
  }

  private boolean isPut(final APIGatewayProxyRequestEvent requestEvent) {
    return PUT.name().equals(requestEvent.getHttpMethod());
  }
}
