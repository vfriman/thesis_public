package com.todo.serverless.handler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.todo.core.domain.TodoTask;

import java.util.UUID;

class TaskDto {
  @JsonProperty
  public UUID task_id;
  @JsonProperty
  public Boolean completed;
  @JsonProperty
  public String description;

  TaskDto(final UUID taskId, final Boolean completed, final String description) {
    this.task_id = taskId;
    this.completed = completed;
    this.description = description;
  }

  @JsonIgnore
  static TaskDto from(final TodoTask todoTask) {
    return new TaskDto(todoTask.getTaskId(), todoTask.getCompleted(), todoTask.getDescription());
  }
}
