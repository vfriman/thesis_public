package com.todo.serverless.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.todo.core.repository.TodoRepository;
import com.todo.core.usecase.*;
import com.todo.serverless.repository.DynamoDbRepo;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.amazonaws.HttpMethod.GET;
import static com.amazonaws.HttpMethod.POST;
import static java.util.stream.Collectors.toList;

public class TodoListHandler implements
  RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  private final Logger logger = LoggerFactory.getLogger(TodoListHandler.class);

  private final Usecase<VoidRequest, CreateTodoListResponse> createTodoList;
  private final Usecase<VoidRequest, FindTodoListsResponse> findTodoLists;

  public TodoListHandler() {
   this(DynamoDbRepo.getInstance());
  }

  TodoListHandler(final TodoRepository todoRepository) {
    this.createTodoList = TodoUsecaseFactory.createTodoList(todoRepository);
    this.findTodoLists = TodoUsecaseFactory.findTodoLists(todoRepository);
  }

  @Override
  public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent requestEvent, final Context context) {
    logger.info("request_event {}", requestEvent);

    if (isPost(requestEvent)) {
      UUID listId = this.createTodoList.handle(VoidRequest.getInstance()).result().get();
      Map<String, String> headers = new HashMap<>();
      headers.put("Location", "/list/" + listId.toString());

      return new APIGatewayProxyResponseEvent()
        .withHeaders(headers)
        .withStatusCode(HttpStatus.SC_CREATED);
    } else if (isGet(requestEvent)) {
      var listdto = this.findTodoLists.handle(VoidRequest.getInstance()).result().get()
        .stream().map(TodoListDTO::from).collect(toList());

      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      return new APIGatewayProxyResponseEvent().withStatusCode(200).withBody(gson.toJson(listdto));
    } else {
      return new APIGatewayProxyResponseEvent()
        .withStatusCode(HttpStatus.SC_BAD_REQUEST)
        .withBody(String.format("{\"error_message\": \" Method %s not supported\" }", requestEvent.getHttpMethod()));
    }
  }

  private boolean isPost(final APIGatewayProxyRequestEvent requestEvent) {
    return POST.name().equals(requestEvent.getHttpMethod());
  }

  private boolean isGet(final APIGatewayProxyRequestEvent requestEvent) {
    return GET.name().equals(requestEvent.getHttpMethod());
  }
}
