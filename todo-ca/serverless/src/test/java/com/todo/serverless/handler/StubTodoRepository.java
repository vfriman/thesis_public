package com.todo.serverless.handler;

import com.todo.core.domain.TodoList;
import com.todo.core.repository.TodoRepository;

import java.util.*;

class StubTodoRepository implements TodoRepository {

  private final Map<UUID, TodoList> todoMap = new HashMap();

  @Override
  public void save(final TodoList todoList) {
    this.todoMap.put(todoList.getListId(), todoList);
  }

  @Override
  public List<TodoList> read() {
    return new ArrayList<>(this.todoMap.values());
  }

  @Override
  public Optional<TodoList> read(final UUID listId) {
    if (this.todoMap.containsKey(listId)) {
      return Optional.of(this.todoMap.get(listId));
    }
    return Optional.empty();
  }
}
